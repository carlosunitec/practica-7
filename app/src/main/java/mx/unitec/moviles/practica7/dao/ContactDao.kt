package mx.unitec.moviles.practica7.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import mx.unitec.moviles.practica7.model.Contact

@Dao
interface ContactDao {
    @Insert
    suspend fun insert(contact: Contact)

    @Delete
    suspend fun delete(vararg contact: Contact)

    @Update
    suspend fun update(vararg contact: Contact)

    @Query("SELECT * FROM " + Contact.TABLE_NAME + " ORDER BY name")
    fun getOrderedAgenda() : LiveData<List<Contact>>
}